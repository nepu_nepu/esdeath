mod functionality;

use std::env;
use slack::{Event, Message, RtmClient};
use regex::Regex;
use lazy_static::lazy_static;
use functionality::math::do_math;
use functionality::search::search_ddg;

struct MyHandler;

#[allow(unused_variables)]
impl slack::EventHandler for MyHandler {
    fn on_event(&mut self, cli: &RtmClient, event: Event) {
        println!("on_event(event: {:?})", event);
        match event {
            Event::Message(message) => {
                // Message is a Box. Its a heap allocated memory.
                let msg = match *message {
                    Message::Standard(msg) => msg,
                    _ => return,
                };
                let channel: String = msg.channel.unwrap();

                parse_message(&msg.text.unwrap(), &channel, cli);
            }
            _ => return,
        }
    }

    fn on_close(&mut self, cli: &RtmClient) {
        println!("on_close");
    }

    fn on_connect(&mut self, cli: &RtmClient) {
        println!("on_connect");
    }
}

fn main() {
    let token = env::var("SLACK_TOKEN").expect("Slack env token must exist");

    let mut handler = MyHandler;
    let r = RtmClient::login_and_run(&token, &mut handler);
    match r {
        Ok(_) => {}
        Err(err) => panic!("Error: {}", err),
    }
}

fn parse_message(text: &str, channel: &str, cli: &RtmClient) {
    // Shove all the regexes in here so they are compiled once lazily. That's neat!
    lazy_static! {
        static ref MATH_RE: Regex = Regex::new(r"\A==(?P<expression>.+)\z").unwrap(); // Eg. `==1+6`
        static ref CHAIN_RE: Regex = Regex::new(r"chainsmokers").unwrap(); // Eg. `I like chainsmokers`
        static ref SEARCH_RE: Regex = Regex::new(r"\Asearch (?P<query>[0-9a-zA-Z ]+)\z").unwrap(); // Eg. `search miku` `search banana`
    }

    let response: Option<String> =
        MATH_RE.captures(text).and_then(|cap| do_math(&cap["expression"]))
    .or_else(||
        CHAIN_RE.find(text).and_then(|_| Some("Wingus".to_string())))
    .or_else(||
        SEARCH_RE.captures(text).and_then(|cap| search_ddg(&cap["query"])))
    .or(None);


    match response {
        Some(msg) => {
            let _ = cli.sender().send_message(&channel, &msg);
        }
        None => return,
    }
}
