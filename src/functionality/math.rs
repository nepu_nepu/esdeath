use calc::eval;

pub fn do_math(possible_math_stuff: &str) -> Option<String> {
    return match eval(possible_math_stuff) {
        Ok(s) => {
            println!("{} is {}", possible_math_stuff, s);
            Some(format!("{}", s))
        }
        Err(_) => None,
    };
}
