use serde::{Deserialize};

// This function only works for queries made from only text, numbers and spaces.
pub fn search_ddg(query: &str) -> Option<String> {

    let plus_query = query.replace(" ", "+");
    // Example Query
    // https://api.duckduckgo.com/?q=DuckDuckGo&format=json
    let final_query = format!("https://api.duckduckgo.com/?q={}&format=json", plus_query);

    // Do the request
    let client = reqwest::Client::new();
    let response = client.get(&final_query).send();

    if response.is_err() {
        return None;
    }

    let mut response = response.unwrap();

    if !response.status().is_success() {
        return None;
    }

    // Get the AbstractURL json field
    let json: DuckResponse = response.json().unwrap();

    if json.AbstractURL.is_empty() {
        return None;
    } else {
        return Some(json.AbstractURL);
    }
}

/// JSON response from duckduckgo.
/// See: https://duckduckgo.com/api
/// Since we only want one field, the others get discared in derserialisation.
#[derive(Deserialize)]
#[allow(non_snake_case)]
struct DuckResponse {
    AbstractURL: String,
}
